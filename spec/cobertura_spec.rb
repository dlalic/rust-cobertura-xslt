# frozen_string_literal: true

require "spec_helper"
require "json"
require "coverage_reports"

RSpec.describe Gitlab::Ci::Parsers::Coverage::Cobertura do
  describe "#parse!" do
    subject(:parse_report) { described_class.new.parse!(cobertura, coverage_report, project_path: project_path, worktree_paths: paths) }

    let(:expected) { File.read("spec/fixtures/expected.json") }
    let(:coverage_report) { Gitlab::Ci::Reports::CoverageReports.new }
    let(:project_path) { "foo/bar" }
    let(:paths) { ["app/user.rb"] }

    context "when input is a modified grcov file" do
      let(:cobertura) { File.read("spec/fixtures/modified-grcov-output.xml") }

      it "works as expected" do
        expect { parse_report }.not_to raise_error
        expect(JSON.pretty_generate(coverage_report.files)).to eq(expected)
      end
    end

    context "when input is an unmodified grcov file" do
      let(:cobertura) { File.read("spec/fixtures/unmodified-grcov-output.xml") }

      it "doesn't work" do
        expect { parse_report }.not_to raise_error
        expect(coverage_report.files).to eq({})
      end
    end

    context "when input is a minimal modified grcov output" do
      let(:cobertura) { File.read("spec/fixtures/minimal-modified.xml") }

      it "works as expected" do
        expect { parse_report }.not_to raise_error
        # note that the lines inside method tag is ignored
        expect(coverage_report.files).to eq({"examples/warn.rs" => {1 => 1}})
      end
    end

    context "when input is a minimal failing grcov output" do
      let(:cobertura) { File.read("spec/fixtures/minimal-failing.xml") }

      it "doesn't work" do
        expect { parse_report }.not_to raise_error
        expect(coverage_report.files).to eq({})
      end
    end
  end
end
