# Rust cobertura xslt
[![pipeline status](https://gitlab.com/dlalic/rust-cobertura-xslt/badges/main/pipeline.svg)](https://gitlab.com/dlalic/rust-cobertura-xslt/-/commits/main)

When using [grcov](https://github.com/mozilla/grcov) to generate a cobertura code coverage report and use it for [Test coverage visualization](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html), GitLab does not display the results. This repository includes excerpts from relevant GitLab code and custom tests to verify that modifying the report works. Since cobertura as a format is not documented, it is open to interpretation as to how it's to be structured.

The problem is that `grcov` puts coverage data in `<lines>` tag inside of `<methods>`, and "orphaned" coverage data in `<lines>` tag inside of `<classes>`. GitLab takes `<lines>` tag inside of `<classes>` as the only source of truth and ignores the rest. An example:

```
<?xml version="1.0"?><!DOCTYPE coverage SYSTEM 'http://cobertura.sourceforge.net/xml/coverage-04.dtd'>
<coverage>
  <sources>
    <source>.</source>
  </sources>
  <packages>
    <package>
      <classes>
        <class>
          <methods>
            <method>
              <lines>
                <line number="1" hits="1">THIS IS IGNORED BY GITLAB</line>
              </lines>
            </method>
          </methods>
          <lines>THIS IS THE SOURCE OF COVERAGE DATA FOR GITLAB</lines>
        </class>
      </classes>
    </package>
  </packages>
</coverage>
```

Hence given the coverage file named `coverage_before.xml`, this operation will modify the report into an output GitLab expects:

```
xmlstarlet ed -d coverage/packages/package/classes/class/lines coverage_before.xml | xmlstarlet tr transform.xslt > coverage.xml
```

The result from modifying the above example is this:

```
<coverage>
  <sources>
    <source>.</source>
  </sources>
  <packages>
    <package>
      <classes>
        <class>
        <lines><line number="1" hits="1" branch="false" condition-coverage="100%">s</line></lines></class>
      </classes>
    </package>
  </packages>
</coverage>
```

### TLDR: GitLab CI job example

```
coverage:
  image: 'rustlang/rust:nightly-bullseye'
  stage: test
  variables:
    RUSTFLAGS: "-Zinstrument-coverage"
    LLVM_PROFILE_FILE: "coverage-%p-%m.profraw"
  before_script:
    - apt-get update && apt-get install -y --no-install-recommends xmlstarlet
    - rustup component add llvm-tools-preview
    - curl -L https://github.com/mozilla/grcov/releases/latest/download/grcov-linux-x86_64.tar.bz2 | tar jxf -
    - curl -O https://gitlab.com/dlalic/rust-cobertura-xslt/-/raw/main/transform.xslt
  script:
    - cargo +nightly test --no-fail-fast || true
    - >
      ./grcov .
      --source-dir .
      --binary-path target/debug
      --branch
      --ignore-not-existing
      --ignore "*cargo*"
      --output-type cobertura
      --output-path coverage_before.xml
    - xmlstarlet ed -d coverage/packages/package/classes/class/lines coverage_before.xml | xmlstarlet tr transform.xslt > coverage.xml
  allow_failure: true
  artifacts:
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
    expire_in: 1 week
    reports:
      cobertura: coverage.xml
```
